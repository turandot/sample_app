require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  
  test "should get home" do
    get :home
    assert_response :success
    assert_select   "title", full_title
  end
  
  test "should get help, about and contact" do
    %w(help about contact).each do |page|
      get page.to_sym
      assert_response :success
      assert_select "title", full_title(page.capitalize)
    end
  end
  
end
