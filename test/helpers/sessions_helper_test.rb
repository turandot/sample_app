require 'test_helper'

class SessionsHelperTest < ActionView::TestCase
  
  def setup
    @user = users(:michael)
    remember(@user)
  end
  
  # Tests the remember branch of current_user because remember method doesn't
  # set sessions[:user_id].
  test "current user returns right user when session is nil" do
    assert_equal @user, current_user
    assert is_logged_in?
  end
  
  # Tests the authenticated? method.
  test "current user returns nil when remember digest is wrong" do
    @user.update_attribute(:remember_digest, User.digest(User.new_token))
    assert_nil current_user
  end
end